const express = require("express");
const serverless = require("serverless-http");
const multer = require("multer");
const path = require("path");

const app = express();
const router = express.Router();

let products = [
  {
    id: 1,
    name: "Product 1",
    photo: "../public/uploads/carica.jpeg",
    purchasePrice: 10,
    sellingPrice: 20,
    stock: 100,
  },
  {
    id: 2,
    name: "Product 2",
    photo: "product2.jpg",
    purchasePrice: 15,
    sellingPrice: 25,
    stock: 50,
  },
];

// Multer configuration
const storage = multer.diskStorage({
  destination: path.join(__dirname, "public/uploads"),
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    const extension = path.extname(file.originalname);
    cb(null, uniqueSuffix + extension);
  },
});

const upload = multer({
  storage,
  limits: {
    fileSize: 100000, // 100KB
  },
  fileFilter: (req, file, cb) => {
    const allowedExtensions = [".jpg", ".jpeg", ".png"];
    const extension = path.extname(file.originalname).toLowerCase();
    if (allowedExtensions.includes(extension)) {
      cb(null, true);
    } else {
      cb(new Error("Invalid file type. Only JPG and PNG are allowed."));
    }
  },
});

router.get("/", (req, res) => {
  res.json({
    hello: "hi!",
  });
});

// Get all products
router.get("/products", (req, res) => {
  res.json({ data: products });
});

// Get a single product
router.get("/products/:id", (req, res) => {
  const productId = parseInt(req.params.id);

  // Find product by ID
  const product = products.find((product) => product.id === productId);

  if (product) {
    res.json({ data: product });
  } else {
    res.status(404).json({ message: "Product not found" });
  }
});

// Create a new product
router.post("/products", (req, res) => {
  const { name, purchasePrice, sellingPrice, stock } = req.body;

  // Check if the product name is unique
  const existingProduct = products.find((product) => product.name === name);
  if (existingProduct) {
    return res.status(400).json({ message: "Product name must be unique" });
  }

  // Validate price and stock fields
  if (
    !Number.isInteger(purchasePrice) ||
    !Number.isInteger(sellingPrice) ||
    !Number.isInteger(stock)
  ) {
    return res.status(400).json({ message: "Invalid price or stock value" });
  }

  // Generate a new ID
  const newProductId = products.length + 1;

  // Create the new product object
  const newProduct = {
    id: newProductId,
    name,
    purchasePrice,
    sellingPrice,
    stock,
  };

  // Add the new product to the products array
  products.push(newProduct);

  res.status(201).json({ data: newProduct });
});

// Upload a product photo
router.post("/products/:id/photo", upload.single("photo"), (req, res) => {
  const productId = parseInt(req.params.id);

  // Find the index of the product
  const productIndex = products.findIndex(
    (product) => product.id === productId
  );

  if (productIndex !== -1) {
    // Check if a file is uploaded
    if (!req.file) {
      return res.status(400).json({ message: "No file uploaded" });
    }

    // Update the product's photo
    products[productIndex].photo = req.file.filename;

    res.json({ message: "Product photo uploaded" });
  } else {
    res.status(404).json({ message: "Product not found" });
  }
});

// Update a product
router.put("/products/:id", (req, res) => {
  const productId = parseInt(req.params.id);
  const { name, purchasePrice, sellingPrice, stock } = req.body;

  // Find the index of the product to be updated
  const productIndex = products.findIndex(
    (product) => product.id === productId
  );

  if (productIndex !== -1) {
    // Check if the product name is unique
    const existingProduct = products.find(
      (product) => product.name === name && product.id !== productId
    );
    if (existingProduct) {
      return res.status(400).json({ message: "Product name must be unique" });
    }

    // Validate price and stock fields
    if (
      !Number.isInteger(purchasePrice) ||
      !Number.isInteger(sellingPrice) ||
      !Number.isInteger(stock)
    ) {
      return res.status(400).json({ message: "Invalid price or stock value" });
    }

    // Update the product
    products[productIndex].name = name;
    products[productIndex].purchasePrice = purchasePrice;
    products[productIndex].sellingPrice = sellingPrice;
    products[productIndex].stock = stock;

    res.json({ data: products[productIndex] });
  } else {
    res.status(404).json({ message: "Product not found" });
  }
});

// Delete a product
router.delete("/products/:id", (req, res) => {
  const productId = parseInt(req.params.id);

  // Find the index of the product to be deleted
  const productIndex = products.findIndex(
    (product) => product.id === productId
  );

  if (productIndex !== -1) {
    // Remove the product from the products array
    const deletedProduct = products.splice(productIndex, 1);

    res.json({ message: "Product deleted", data: deletedProduct });
  } else {
    res.status(404).json({ message: "Product not found" });
  }
});

app.use(express.json());
app.use(`/.netlify/functions/api`, router);

module.exports = app;
module.exports.handler = serverless(app);
